
# Overview

This package contains the Python version of a simple discrete event simulator that I wrote to perform
simulations of scheduling of clusters. The general approach is that event handler functions or methods are
registered with a simulator, initial events are added to the simulator, the simulator is started, handlers add
additional events to the simulator as they handle events, and the simulator stops when there are no more
events to handle.

The primary goal is to perform simulations as fast as possible so that weeks of simulation time can be
accomplished in minutes. I call this "event time" and it is implemented by the EventTimeSimulator class in the
sim module.  This package can also perform simulations in "real time" where 1 minute of simulation is
performed in as close to 1 minute of wall time as a single thread can do. This approach is implemented by the
RealTimeSimulator in the sim module.

example/pingpong.py contains a simple example of performing event time or real time simulations.

# License

This software is licensed under the BSD 3-clause license.
