
from setuptools import find_packages, setup

def readme():
    with open("README.md") as f:
        return f.read()

if __name__ == "__main__":
    setup(name="esim",
          version="0.1",
          description="An discrete event simulator",
          long_description=readme(),
          classifiers=[
              "Development Status :: 4 - Beta",
              "License :: OSI Approved :: BSD License",
              "Programming Language :: Python :: 2",
              "Topic :: System :: Emulators",
          ],
          keywords="discrete event simulator",
          url="https://bitbucket.org/wwsmith/esim-python",
          author="Warren Smith",
          author_email="wsmith@tacc.utexas.edu",
          license="BSD",
          packages=["esim","esim.example"],
          include_package_data=False,
          zip_safe=False)
