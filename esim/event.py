
import logging

class Event(object):
    """The Event class and it's subclasses describe events that have occurred. It is expected that subclasses of
    this class will be implemented that contain additional information about application-specific events. """

    cur_event_id = 1

    def __init__(self, time=0, priority=0):
        self.time = time
	self.priority = priority
	self.event_id = Event.cur_event_id
	Event.cur_event_id += 1

    def __str__(self):
        str = "event %d of type %s:\n" % (self.event_id, self.__class__.__name__)
	str += "  time: %f\n" % self.time
	str += "  priority: %f\n" % self.priority
	return str

    def __cmp__(self, other):
        if self.time < other.time:
            return -1
        elif self.time > other.time:
            return 1
        if self.priority < other.priority:
            return -1
        elif self.priority > other.priority:
            return 1
        return self.event_id - other.event_id

    def __eq__(self, other):
        return self.event_id == other.event_id

    def handler_key(self):
        return  "%s.%s" % (self.__module__, self.__class__.__name__)
