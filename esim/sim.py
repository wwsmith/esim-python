
from heapq import heappush, heappop
import logging
import threading
import time

#######################################################################################################################

class Simulator(object):
    """A general interface for a discrete event simulator. An interface was defined since there can be a number of
    different implementations."""

    def __init__(self):
        self.queue = []
        self.handlers = {}

    def cur_time(self):
        """Returns the current time. No units are specified, as the units are dependent on what is being simulated."""

        raise NotImplementedError

    def add_handler(self, event, handler):
        """Registers an event handler function for events of class event_cls. An event handler function takes two
        arguments: the simulator and the event."""

        logging.debug("add handler for %s" % event.handler_key());
        if event.handler_key() not in self.handlers:
            self.handlers[event.handler_key()] = []
        self.handlers[event.handler_key()].append(handler)

    def add_event(self, event):
        """This method adds a new event to the Simulator. The Simulator will then dispatch this event to
        appropriate handlers at the appropriate time."""

        logging.debug("add event %s" % event)
        heappush(self.queue, event)

    def run(self, until=None):
        """This method is used to run the simulation until their are no more events to handle or until a specific
        simulation time is reached. Before calling this method, one or more events must be added to the
        simulator using add_event(). The handlers that receive these events while this method is executing add
        more events via add_event(). And so on. And so on..."""

        raise NotImplementedError

#######################################################################################################################

class EventTimeSimulator(Simulator):
    """This class performs a simulation in "event time". It dispatches events as quickly as possible in real time
    while keeping track of the current simulated time. So, you could simulate days or weeks of activity in a
    few minutes."""

    def __init__(self):
        Simulator.__init__(self)
        self.time = 0

    def __str__(self):
        str = "EventTimeSimulator at time %d has events:\n" % (self.time)
        for event in queue:
            str += "  %s" % event
        return str

    def cur_time(self):
        return self.time

    def _dispatch(self, event):
        logging.debug("dispatch %s" % event)
        try:
            for handler in self.handlers[event.handler_key()]:
                handler(self, event)
        except KeyError:
            logging.warn("no handler for event %s" % event.__class__.__name__)

    def run(self, until=None):
        while len(self.queue) > 0:
            event = heappop(self.queue)
            if self.time > event.time:
                logging.error("time %d is greater than time for next event %s" % (time, event))
            self.time = event.time
            self._dispatch(event)

#######################################################################################################################

class RealTimeSimulator(Simulator):
    """This class performs a simulation in "real time". By this I mean it dispatches events at the time each event
    specifies that it should be dispatched (as closely as possible). This simulator is useful in situations
    such as when you want to apply a specific load to a system for performance testing.

    Note that for this simulator, the values for times in Events are interpreted as seconds since the start of
    the simulation. Fractions of a second area also allowed."""

    def __init__(self):
        Simulator.__init__(self)
        self.time = 0
        self.start_time = 0
        self.delay_factor = 0.01
        self.cond = threading.Condition()

    def __str__(self):
        str = "RealTimeSimulator at time %d has events:\n" % (self.time)
        for event in queue:
            str += "  %s" % event
        return str

    def cur_time(self):
        return time.time() - self.start_time

    def _dispatch(self, event):
	logging.debug("dispatching event: %s" % event)
	error = self.cur_time() - event.time
	self.delay_factor = 0.9 * self.delay_factor + 0.1 * error # adaptively adjust to reduce error

        try:
            for handler in self.handlers[event.handler_key()]:
                handler(self, event)
        except KeyError:
            logging.warn("no handler for event %s" % event.__class__.__name__)

    def run(self):
        self.start_time = time.time();
        while len(self.queue) > 0:
            event = self.queue[0]
            cur_time = self.cur_time()
            if event.time <= cur_time:
                heappop(self.queue)
                self._dispatch(event)
            else:
                self.cond.acquire()
                self.cond.wait(event.time - cur_time - self.delay_factor)

#######################################################################################################################
