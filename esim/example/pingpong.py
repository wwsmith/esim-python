
import sys
import time
import logging
from esim import *

class PingPong(object):
    def __init__(self, simulator, total_pings=1000, delay=0.1):
        self.simulator = simulator
        self.total_pings = total_pings
        self.num_pings = 0
        self.delay = delay

    def run(self):
        simulator.add_handler(PingEvent(), self.handle_ping)
        simulator.add_handler(PongEvent(), self.handle_pong)

        ping_event = PingEvent(1)
        simulator.add_event(ping_event)

        start = time.time()
        simulator.run()
        stop = time.time()

        diff = stop - start

        print "\n%d pingpongs in %f seconds or %f events per second\n" % (num_pings, diff, num_pings*2.0/diff)

    def handle_ping(self, simulator, event):
        self.num_pings += 1
        logging.debug("sending Ping %d" % self.num_pings)
        if self.num_pings < self.total_pings:
            simulator.add_event(PongEvent(event.time + self.delay))

    def handle_pong(self, simulator, event):
        logging.debug("sending Pong")
        simulator.add_event(PingEvent(event.time + self.delay))

class PingEvent(Event):
    def __init__(self, time=0, priority=0):
        Event.__init__(self, time, priority)

class PongEvent(Event):
    def __init__(self, time=0, priority=0):
        Event.__init__(self, time, priority)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        num_pings = int(sys.argv[1])
    else:
        num_pings = 1000

    delay = 0.1
    simulator = EventTimeSimulator()
    if len(sys.argv) > 2:
        if sys.argv[2] == "eventtime":
            pass # already created
        elif sys.argv[2] == "realtime":
            simulator = RealTimeSimulator()
            if len(sys.argv) > 3:
                delay = int(sys.argv[3])
        else:
            print("argument 2 should be either 'eventtime' or 'realtime'")
            exit(1)

    pingpong = PingPong(simulator, num_pings, delay)
    pingpong.run()
